<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<form method="post" action=""
  enctype="multipart/form-data">
    <input type="text" name="max">
    <input type="submit" name="送信">
</form>
<hr>
<?php
/*
for ($i=1;$i<=100;$i++) {
    if ( $i % 3 == 0 && $i % 5 == 0 ) {
        // 3かつ5の倍数のとき
        echo 'fifteen ';
    } elseif ($i % 3 == 0) {
        // 3の倍数のとき
        echo 'three ';
    } elseif ($i % 5 == 0) {
        // 5の倍数のとき
        echo 'five ';
    } else {
        // 他の値の場合のとき
        echo $i. ' ';
    }
}
 */
function three_five($number) {
    if ($number % 3 == 0 && $number % 5 == 0) {
        // 3かつ5の倍数のとき
        return 'fifteen';
    } elseif ($number % 3 == 0) {
        // 3の倍数のとき
        return 'three';
    } elseif ($number % 5 == 0) {
        // 5の倍数のとき
        return 'five';
    } else {
        // 他の値の場合のとき
        return $number;
    }
}

$max = (isset( $_POST['max'] ))
        ? $_POST['max'] : 0;

for ($i = 1; $i <= $max; $i++) {
    echo three_five($i). ' ';
}


/*

echo three_five(1).' ';
echo three_five(2).' ';
echo three_five(3).' ';
echo '<br><br>';

 */
?>
    </body>
</html>
