<?php
// まず、セッションが使える状態にする
session_set_cookie_params(3600);
session_start();

// GETデータがセットされていた場合、
// 　その内容をセッションに保存
if (isset($_GET['name'])) {
    $_SESSION['name'] = $_GET['name'];
}

// 画面に $_SESSION['name'] の内容を表示
echo $_SESSION['name'];
