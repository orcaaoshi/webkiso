<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

$names = [
            '浅野',    // 0
            '伊藤',    // 1
            '宇田',    // 2
            '江本',    // 3
         ];

echo $names[0].'<br>';
echo $names[1].'<br>';
echo $names[2].'<br>';
echo $names[3].'<br>';
echo $names[4].'<br>';

echo '<pre>';
var_dump($names);
echo '</pre>';

?>
    </body>
</html>
