<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
// 変数$aはグローバルスコープの変数です。
$a = 'グローバル$a';
// 変数$bもグローバル変数です。
$b = 'グローバル$b';

function test()
{
    // ここの変数$aは、この関数内でのみ有効
    // ローカルスコープ変数。
    // 名前が同じでもグローバル変数$a とは別物。
    $a = 'ローカル$a';
    echo $a. '<br>';
    
    // 関数内からグローバル変数を使いたい場合は
    // 明示的にglobalキーワードを使う
    global $b;
    $b = 'ローカル$b';
    echo $b. '<br>';
}

test();
echo $a . '<br>';
echo $b . '<br>';
?>
    </body>
</html>
