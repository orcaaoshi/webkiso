<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>変数と文字列の出力</title>
    </head>
    <body>
<?php
// 変数$nameに文字列を代入します
$name = 'PHP逆引きレシピ';
//
$Name = '徹底入門';

// 
echo '書籍名: '. $name. '<br>';
echo "書籍名: {$Name}\n<br>";
echo '書籍名: {$Name}\n<br>';

$format = '書籍名: %s、%s<br>';
echo sprintf($format, $name, $Name);



// 複数行の文字列
$book = 'PHP逆引きレシピ';
$text = <<<EOL
ヒアドキュメント<br>
書籍名： $book<br>
EOL;

echo $text;

echo <<<END
echoも
できます<br><br>
END;

echo <<<'END'
Nowdoc構文です<br>
変数は展開されません。<br>
書籍名: $book<br>
END;

?>
    </body>
</html>

