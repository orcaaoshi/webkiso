<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
// 変数に整数を代入します。
$a = 123;
// 変数に文字列を代入します。
$b = '321';

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

// 関数を使い、型を変換します。
$a = strval($a);
$b = intval($b);

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

// 型キャストで型を変換します。
$a = (int) $a;
$b = (string) $b;

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';


$c = '1,234';
$c = (int) $c;

echo '$cの型: ' . gettype($c) . '<br>';
echo '$cの中身: ' . $c . '<br><br>';
?>
    </body>
</html>
