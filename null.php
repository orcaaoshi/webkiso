<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$a = null;
$b = '';

echo $a. '<br>';
echo $b. '<br>';

if ($a == $b) {
    echo '等しい(==)'. '<br>';
} else {
    echo '異なる(==)'. '<br>';
}

if ($a === $b) {
    echo '等しい(===)'. '<br>';
} else {
    echo '異なる(===)'. '<br>';
}   

if (is_null($a) ) {
    echo 'nullです'. '<br>';
} else {
    echo 'nullではない'. '<br>';
}   

if (isset($a)) {
    echo '定義されています'. '<br>';
} else {
    echo '定義されていません'. '<br>';
}   

?>
    </body>
</html>
