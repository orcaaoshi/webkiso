<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
// 九九を保存
$kukus = array();
for($i = 1; $i <= 9; $i++) {
    $dan = array();  // ポイント！
    for($j = 1; $j <= 9; $j++) {
        $dan[] = $i * $j;
    }
    $kukus[] = $dan;
}

/*
echo '<pre>';
var_dump($kukus);
echo '</pre>';
*/

// 保存しておいた九九を表示
for ($i = 0 ; $i < count($kukus) ; $i++) {
    for ($j = 0 ; $j < count($kukus[$i]) ; $j++) {
        echo $kukus[$i][$j]. ', ';
    }
    echo '<br>';
}

?>
    </body>
</html>
