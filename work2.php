<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
// 年齢を代入
$age = 34;

echo "わたしは" . $age . "歳です。来年は". ($age + 1) ."歳です。<br>";
echo "わたしは{$age}歳です。来年は" . ++$age . "歳です。<br>";

//
$age = 20.5;
echo (int) $age;
?>
    </body>
</html>
