<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$kanas = [
            'あいうえお',
            'かきくけこ',
            'さしすせそ',
            'たちつてと',
            'なにぬねの',
          ];
$count = (is_array($kanas)) ?
         count($kanas) : 0;
for($i=0;$i<$count;$i++){
    echo $kanas[$i];
}

// は行とま行を追加する
$kanas[] = 'はひふへほ';
$kanas[] = 'まみむめも';

// 最後の要素を表示する
echo '<br>';
echo $kanas[ count($kanas) - 1 ];

?>
    </body>
</html>
