<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$weeks = [
            '日曜',
            '月曜',
            '火曜',
            '水曜',
            '木曜',
            '金曜',
            '土曜',
         ];

$timestamp = mktime(0,0,0,6,1,2017);
$week = date('w', $timestamp);

echo date('Y年n月j日は、', $timestamp).
     $weeks[$week]. '日です';

?>
    </body>
</html>
