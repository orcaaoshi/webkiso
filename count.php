<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$names = [
            '浅野',    // 0
            '伊藤',    // 1
            '宇田',    // 2
            '江本',    // 3
         ];
echo count($names);
echo '<br>';

echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';

$names[] = '太田';
echo count($names);
echo '<br>';

echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';

echo $names[ count($names) -1 ];
echo '<br>';



$empty = [];
echo count($empty);
echo '<br>';

$test = 100;
if (is_array($test)) {
    echo count($test);
} else {
    echo '配列ではありません';
}

echo '<br>';

$test = [100];
if (is_array($test)) {
    echo count($test);
} else {
    echo '配列ではありません';
}

//echo count($test);
echo '<br>';

$test = 100;
$count = (is_array($test)) ?
         count($test) : '×';
echo $count;

?>
    </body>
</html>
