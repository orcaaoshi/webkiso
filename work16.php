<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
// ①
$result = 0;
$result_even = 0;
for($i=1; $i<=100 ;$i++) {
    $result += $i;

    if ( $i % 2 == 0 ) {  // 偶数は2で割り切れる
        $result_even += $i;
    }
}
echo '1〜100まで全て足すと ' . $result . '<br>';
echo '1〜100まで偶数だけ足すと ' . $result_even . '<br>';

// ②
$result = 0;
for($i=0; $i<=100 ;$i+=2) {
    $result += $i;
}
echo '1〜100まで偶数だけ足すと ' . $result . '<br>';

?>
    </body>
</html>
