<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$timestamp = mktime(0,0,0,6,16,2017);
$week = date('w', $timestamp);

$message = ($week >= 1 && $week <= 5) ? '平日です' : 'おやすみです';
/*
 または
$message = (         ) ? 'おやすみです' : '平日です' ;
*/

echo $message;
?>
    </body>
</html>
