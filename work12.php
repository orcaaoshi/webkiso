<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$timestamp = mktime(0,0,0,6,21,2017);
$week = date('w', $timestamp);

switch ($week) {
    case 1:
        echo '月曜日です';
        break;
    case 2:
        echo '火曜日です';
        break;
    case 3:
        echo '水曜日です';
        break;
    case 4:
        echo '木曜日です';
        break;
    case 5:
        echo '金曜日です';
        break;        
    case 0:
    case 6:
        echo 'おやすみです';
        break;            
    default:
        echo '正しくない値が渡されました';
        break;
}

echo '<br><br>';

if ($week == 1) {
    echo '月曜日です';
} elseif ($week == 2) {
    echo '火曜日です';
} elseif ($week == 3) {
    echo '水曜日です';
} elseif ($week == 4) {
    echo '木曜日です';
} elseif ($week == 5) {
    echo '金曜日です';
} else {
    echo 'おやすみです';
}


?>
    </body>
</html>
